//
//  SecondViewController.swift
//  webcheckapp
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController , UITextFieldDelegate{
    
    
    @IBOutlet weak var itemText: UITextField!
    
    @IBAction func addItem(sender: AnyObject) { // action on button event
      if itemText.text != "" {
        self.view.endEditing(true)  // hide keyboard on button touch
        
        toDoItems.append(itemText.text)
        
        let storedItem = toDoItems
        
        NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "toDoItems")
        
        NSUserDefaults.standardUserDefaults().synchronize()
        
        itemText.text = ""
      }
      else {
         return
      }
      
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool { // func to hide keyboard on pressing return button
        
        itemText.resignFirstResponder()
      
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) { // func to hide keyboard on touching outside
        
        self.view.endEditing(true)

         }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemText.becomeFirstResponder()
      
      // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
      
      super.didReceiveMemoryWarning()
      
      // Dispose of any resources that can be recreated.
    }


}

