//
//  FirstViewController.swift
//  webcheckapp
//
//  Created by Click Labs on 1/20/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

var toDoItems :[String] = []      //global variable array

class FirstViewController: UIViewController, UITableViewDelegate
            {

    @IBOutlet var tasksTable :UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int  // return no of table cell
    {
      return toDoItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) ->UITableViewCell   {   // setting the text label in the cell
        
          var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
          cell.textLabel?.text = toDoItems[indexPath.row]
      
          return cell
  
    }
    
        override func viewWillAppear(animated: Bool) { //setting the initial data load
        
            if var itemInMemory :AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("toDoItems"){
            
              toDoItems = []
              
              for var i = 0 ; i < itemInMemory.count; ++i {
            
                toDoItems.append(itemInMemory[i] as NSString)
              }
            }
       
            tasksTable.reloadData()
        }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        // function for deleting the todo item
        if editingStyle == UITableViewCellEditingStyle.Delete {
           
            toDoItems.removeAtIndex(indexPath.row)
          
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
          
            tasksTable.reloadData()
           
            let storedItem = toDoItems
            
            NSUserDefaults.standardUserDefaults().setObject(storedItem, forKey: "toDoItems")
            
            NSUserDefaults.standardUserDefaults().synchronize()
          
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
          
          //return the table height
          
          return tableView.rowHeight;
    }
  
  
  
    func colorForIndex(index: Int) -> UIColor { // user defined function for creating color
        
        let itemCount = toDoItems.count - 1
        
        let val = (CGFloat(index) / CGFloat(itemCount)) * 0.6
        
        return UIColor(red: 1.0, green: val , blue: 0.0, alpha: 1.0)
    }
  
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,forRowAtIndexPath indexPath: NSIndexPath) {
      
      // setting the different color  to each tablecell
            cell.backgroundColor = colorForIndex(indexPath.row)
        
    }
 
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

